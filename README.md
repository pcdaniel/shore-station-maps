# Shore Station Map #

Author: P. Daniel

Maps are made using cartopy and matplotlib. The Bathymetry data comes from Natural Earth (coarse resolution) and Costal Relief Maps (fine resolution). Map insets are patched together in adobe illustrator.

![](./figures/shore-station-map.png)
